﻿using UnityEngine;
using System.Collections;

public class Teleporter : MonoBehaviour {


    public Transform exitPoint;
    private bool hasPoint;

	
	// Update is called once per frame
	void Awake () {

        if (exitPoint != null)
        {
            hasPoint = true;
        }


	}




    void OnTriggerEnter2D(Collider2D d)
    {
        if (hasPoint)
        {

            if (d.gameObject.CompareTag("Player") || d.gameObject.CompareTag("NPC"))
            {

                if(d.gameObject.CompareTag("Player"))
                {
                    d.gameObject.SendMessage("ResetColliders");
                }

                if (d.gameObject.CompareTag("NPC"))
                {
                    d.SendMessage("Reset");
                }


                Instantiate(Resources.Load("TeleEnterFX"), d.transform.position, Quaternion.identity);
                d.gameObject.transform.position = exitPoint.position;
                Instantiate(Resources.Load("TeleExitFX"), exitPoint.transform.position, Quaternion.identity);
                
            }



        }
       
            
        

    }

}
