﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Typer : MonoBehaviour {

	// Use this for initialization

    private Text cachetext;
    private string text;
    public bool hasFinnished = false;
    private char[] buffer;
    public float delay=0.2f;
    private float timer;
    private bool stop;
    public int index=0;
    private string dialogue="";
    public bool started=false;
    public bool resetFlag = false;
	void Awake () {
        Debug.Log("TYPE");
        cachetext = GetComponent<Text>();
        text = cachetext.text;
        cachetext.text = "";
        buffer=text.ToCharArray(0,text.Length);
	}
	
	// Update is called once per frame
    void Update()
    {

                
            if (started)
            {
                if (!hasFinnished)
                {
                    if (!stop)
                    {
                        dialogue += buffer[index];
                        cachetext.text = dialogue;
                        index++;
                        timer = 0;
                        stop = true;
                    }
                    else
                    {
                        timer += Time.deltaTime;

                        if (timer >= delay)
                        {
                            stop = false;
                        }
                    }

                    if (index >= buffer.Length)
                    {
                        Debug.Log("END");
                        hasFinnished = true;
                    }



                }
        

       


        }
    }

    public void Reset()
    {
       
        cachetext.text = "";
        dialogue = "";
        hasFinnished = false;
        stop = false;
        index = 0;
        timer = 0;
    }

}
