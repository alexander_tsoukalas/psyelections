﻿using UnityEngine;
using System.Collections;

public class TeleExitFX : MonoBehaviour {



    ParticleSystem a;
    
    
    // Use this for initialization
	void Awake () {


        a = GetComponentInChildren<ParticleSystem>();

        a.Play();

	}
	
	// Update is called once per frame
	void Update () {

        if (a.isStopped)
        {
            Destroy(gameObject);
        }



	}
}
