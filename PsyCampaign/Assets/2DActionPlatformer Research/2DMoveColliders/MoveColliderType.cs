﻿using UnityEngine;
using System.Collections;

public enum MoveColliderType { CEILING, GROUND, LEFT, RIGHT }
