﻿using UnityEngine;
using System.Collections;

public class Switch : MonoBehaviour {



    public bool isTriggered = false;
    private bool startTimer = false;
    public float delay=2;
    private float timer=0;
    private Animator anim;

    public Transform sensor1;
    public Transform sensor2;
    public Transform sensor3;

    private SpecialSensor s1;
    private SpecialSensor s2;
    private SpecialSensor s3;

    private bool ready = false;



    int i = 0;
    bool stepped=false;

	void Awake () {

        anim = GetComponentInParent<Animator>();


        if (sensor1 != null && sensor2 != null && sensor3 != null)
        {



            s1 = sensor1.GetComponent<SpecialSensor>();
            s2 = sensor2.GetComponent<SpecialSensor>();
            s3 = sensor3.GetComponent<SpecialSensor>();
            ready = true;

        }




	}
	
	// Update is called once per frame
	void Update () {

        if (ready)
        {
            //Debug.Log(s1.triggered + "||" + s2.triggered + "||" + s3.triggered);


            if (s1.triggered && s2.triggered && s3.triggered)
            {
                isTriggered = true;
                anim.SetTrigger("Activate");
            }


            


            //if (s1.triggered && s2.triggered && s3.triggered)
            //{
            //    isTriggered = true;

            //}
            //else
            //{
            //    isTriggered = false;

            //}



        }


        


	}
}
