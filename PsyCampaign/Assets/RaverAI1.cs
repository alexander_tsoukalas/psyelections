﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RaverAI1 : MonoBehaviour {

	// Use this for initialization


    public Transform sensorTran;

    private bool hasSensor = false;
    private AISensor sensor;
    public bool follow = false;
    GameObject player;

    public float velX;
    public float xAccel = 1f;
    public float maxX=20;
    private Vector2 lastDir;

    public List<MoveColliderCheck> leftColliders = new List<MoveColliderCheck>();
    public List<MoveColliderCheck> rightColliders = new List<MoveColliderCheck>();

       
    public bool[] leftColResults;
    public bool[] rightColResults;


    public bool leftFinal=false;
    public bool rightFinal = false;

	void Awake () {

        InitialiseColliders();
        if (sensorTran != null)
        {
            hasSensor = true;
            sensor = sensorTran.GetComponent<AISensor>();
            player = GameObject.FindGameObjectWithTag("Player");
        }


	}


    void Update()
    {

        CheckMoveColliders();

        if (GameManager.glowstick)
        {

            follow = true;
        }
        else
        {
            follow = false;

        }

    }



	// Update is called once per frame
	void FixedUpdate () {

        Vector2 d = player.transform.position - transform.position;
        float m = d.magnitude;

        d.Normalize();

//        Debug.Log("DIS=" + d + ", M=" + m);

        Vector2 rV = GetComponent<Rigidbody2D>().velocity;

        if (hasSensor)
        {


            if (sensor.detect)
            {
             

                if (follow)
                {
                    lastDir = d;

                    if (m > 10)
                    {

                        velX += d.x * xAccel;
                        rV.x = velX;
                    }
                    else if (m > 6 && m < 10)
                    {
                        velX += d.x * (xAccel / 3);
                        rV.x = velX;
                    }
                    else if (m < 5)
                    {

                        velX -= d.x * (xAccel / 3);

                        if (Mathf.Abs(velX) <= 0.1f)
                        {
                            velX = 0;
                        }


                    }

                   // rigidbody2D.velocity = rV;


                }

                else
                {
                    if (Mathf.Abs(velX) > 0.5f)
                    {


                        velX -= Mathf.Sign(velX) * xAccel;



                    }
                    else
                    {
                        velX = 0;
                    }

                    
                }
                
            }
                //END FOLLOW TRUE


            else
            {



                if (Mathf.Abs(velX) > 0.5f)
                {


                    velX -= Mathf.Sign(velX) * xAccel;



                }
                else
                {
                    velX = 0;
                }


                //Debug.Log("SLOWDOWN=" + velX);

                
            }


        }



        if (Mathf.Abs(velX) > maxX)
        {

            velX = Mathf.Sign(velX)*maxX;
            

        }



        if (lastDir.x < 0)
        {
            Debug.Log("LEFT");

            if (leftFinal)
            {
                velX = 0;
            }
        }
        else if (lastDir.x > 0)
        {
            Debug.Log("RIGHT");

            if (rightFinal)
            {
                velX = 0;
            }
        }
        else
        {
            //Debug.Log("NOT MOVING");
        }

        rV.x = velX;
        GetComponent<Rigidbody2D>().velocity = rV;

	}



    public void Reset()
    {
        sensor.detect = false;
    }



    void InitialiseColliders()
    {
         
        foreach (Transform child in transform)
        {
            if (child.gameObject.CompareTag("MoveCollider"))
            {

                MoveColliderCheck script = child.GetComponent<MoveColliderCheck>();


                switch (script.type)
                {

                    case MoveColliderType.CEILING:

                        
                        break;

                    case MoveColliderType.GROUND:
                                                                        
                        break;

                    case MoveColliderType.LEFT:

                        leftColliders.Add(script);

                        break;

                    case MoveColliderType.RIGHT:
                        rightColliders.Add(script);
                        break;

                }

            }
        }




       
        rightColResults = new bool[rightColliders.ToArray().Length];
        leftColResults = new bool[leftColliders.ToArray().Length];
    
    }

    void CheckMoveColliders()
    {
        int i = 0;

        foreach (MoveColliderCheck check in rightColliders)
        {

            rightColResults[i] = check.hasCollision;
            i++;

        }
        rightFinal = false;


        for (int a = 0; a < rightColResults.Length; a++)
        {

            if (rightColResults[a])
            {


                rightFinal = rightColResults[a];
            }
        }

        //Check Left

        i = 0;
        foreach (MoveColliderCheck check in leftColliders)
        {

            leftColResults[i] = check.hasCollision;
            i++;

        }
        leftFinal = false;


        for (int a = 0; a < leftColResults.Length; a++)
        {

            if (leftColResults[a])
            {


                leftFinal = leftColResults[a];
            }
        }

    }

}


