﻿using UnityEngine;
using System.Collections;

public class SignPost : MonoBehaviour {



    public bool playerEnter=false;
    public Animator anim;
    private bool animPlaying = false;
    private Typer typer;


	// Use this for initialization
	void Awake () {

        anim = GetComponent<Animator>();

        typer = GetComponentInChildren<Typer>();
        //typer = tmp.GetComponent<Typer>();
	}
	
	// Update is called once per frame
	void Update () {

       // Debug.Log(animPlaying);

        if (Input.GetButtonDown("Interact"))
        {
            if (playerEnter)
            {

                if (!animPlaying)
                {


                    typer.Reset();
                    animPlaying = true;
                    anim.SetTrigger("Activate");

                }
                else
                {
                    
                    if (typer.hasFinnished)
                    {
                        Debug.Log(typer.hasFinnished);
                        anim.SetTrigger("Hide");
                        animPlaying = false;
                    }
                }

            }
        
        }

               


	}



    void OnTriggerEnter2D(Collider2D d)
    {
        if (d.gameObject.CompareTag("Player"))
        {
            playerEnter = true;

        }
    }

    void OnTriggerStay2D(Collider2D d)
    {

        if (d.gameObject.CompareTag("Player"))
        {
            playerEnter = true;

        }


    }




    void OnTriggerExit2D(Collider2D d)
    {


        if (d.gameObject.CompareTag("Player"))
        {
            playerEnter = false;

            if (animPlaying)
            {
                anim.SetTrigger("Hide");
                animPlaying = false;
            }


            //        anim.SetTrigger("Hide");
            //        animPlaying = false;
            //        Debug.Log("HIDE");




        }
    }
}
