﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Collections.Generic;


public class TwoDPlatformController : MonoBehaviour
{


    public bool isPaused;
    private float cacheY;

    ArrayList rightRayTranlist = new ArrayList();
    ArrayList leftRayTranlist = new ArrayList();
    ArrayList upRayTranlist = new ArrayList();
    ArrayList downRayTranlist = new ArrayList();



    //NEW SYSTEM-------------------------------------------------------------------------------------------------


    public List<MoveColliderCheck> ceilingColliders = new List<MoveColliderCheck>();
    public List<MoveColliderCheck> groundColliders = new List<MoveColliderCheck>();
    public List<MoveColliderCheck> leftColliders = new List<MoveColliderCheck>();
    public List<MoveColliderCheck> rightColliders = new List<MoveColliderCheck>();



    




    public bool[] groundColResults;
    public bool[] ceilingColResults;
    public bool[] leftColResults;
    public bool[] rightColResults;

    public bool ceilingFinalResult;


    //-----------------------------------------------------------------------------------------------------------
    

    public bool rightRayFinal;
    public bool leftRayFinal;
    public bool downRayFinal;
    public bool upRayFinal;

    //Screens
    public enum States { Normal, Status };

    public States state = States.Normal;

    //Input Triggers
    private bool left;
    private bool right;
    public bool launchJump;


    //Speed Values

    public float velX = 0;
    public float velY = 0;
    public float maxVel = 40;
    public float velAcc = 1.5f;
    public float jumpForce = 1500f;
    public float maxFallSpeed = 100;
    public float jumpHeight = 55;
    public float jumpInclineVel=5.5f;
    public float jumpDeclineVel=2.5f;
    public bool jumpClimax = false;
    public bool reachHeight = false;
    public float jumpDelay=0.10f;
    public float jumpTimer=0;

    //Various Values

    public Vector2 lastDir;
          

    //private Switches
    private bool addJumpForce = false;

    //timers
    private Animator anim1;
        
    private GameObject playerModel;

    private Animator playerAnim;
           

    void Awake()
    {
        //InitializeRays();
        InitializeMoveColliders();
        playerModel = GameObject.FindGameObjectWithTag("PlayerModel");
        playerAnim = playerModel.GetComponentInChildren<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        //Read Input

        if (Input.GetButtonDown("Right"))
        {
            //lastDir = Vector2.right;
            right = true;
        }


        if (Input.GetButtonUp("Right"))
        {

            right = false;

        }

        if (Input.GetButtonDown("Left"))
        {
            // lastDir = -Vector2.right;
            left = true;

        }

        if (Input.GetButtonUp("Left"))
        {
            left = false;
        }

        if (!isPaused)
        {
            switch (state)
            {
                case States.Normal:


                    CheckMoveColliders();



                    //RenderRays();
                    //ComputeRays();
                    //Check rigididbody2D Y velocity
                    velY = GetComponent<Rigidbody2D>().velocity.y;

                    
                    //Manage facing direction
                    
                    if (velX > 1)
                    {
                        //Debug.Log("OVER 0");

                        Quaternion a = playerModel.transform.localRotation;
                        a.y = 0;
                        playerModel.transform.localRotation = a;
                        //t.x = Mathf.Abs(t.x);

                        //playerModel.transform.localScale = t;

                        lastDir = Vector2.right;
                        
                    }
                    else if (velX < -1)
                    {
                        //Vector3 t = playerModel.transform.localScale;

                        //t.x = -Mathf.Abss(t.x);

                        //playerModel.transform.localScale = t;

                        Quaternion a = playerModel.transform.localRotation;
                        a.y = 90;
                        playerModel.transform.localRotation = a;

                        lastDir = -Vector2.right;

                    }
                    else
                    {

                    }
                                                          

                    if (Input.GetButtonDown("Jump"))
                    {

                        if (!launchJump && downRayFinal)
                        {
                            playerAnim.SetTrigger("Jump");
                            launchJump = true;
                        }

                    }

                    break;






                case States.Status:





                    break;


            }

        }

    }



    void FixedUpdate()
    {

               
        if(!isPaused)
        {

            switch (state)
            {

                case States.Normal:

                    //Process Input

                    playerAnim.SetFloat("Velocity", Mathf.Abs(velX));
                    playerAnim.SetBool("Ground", downRayFinal);
                    playerAnim.SetBool("Ceiling", upRayFinal);


                    //playerAnim.SetTrigger("Jump");



                    //Left btn is down and Right btn is not

                    if (left && !right)
                    {

                        //if already moving left

                        if (velX < 0)
                        {
                            if (!leftRayFinal)
                            {
                                if (velX > -maxVel)
                                {
                                    velX -= velAcc;
                                }
                                else
                                {
                                    velX = -maxVel;
                                }
                            }
                            else
                            {
                                velX = 0;
                            }
                        }

                        //if already moving right

                        else if (velX > 0)
                        {
                            if (!rightRayFinal)
                            {
                                velX -= velAcc;
                            }
                            else
                            {
                                velX = 0;
                            }
                        }


                        //if not moving at all
                        else
                        {
                            if (!leftRayFinal)
                            {
                                if (velX > -maxVel)
                                {
                                    velX -= velAcc;
                                }
                                else
                                {
                                    velX = -maxVel;
                                }
                            }
                            else
                            {
                                velX = 0;
                            }

                        }






                    }
                    //Right btn is down and Left btn
                    else if (!left && right)
                    {


                        //If already moving left

                        if (velX < 0)
                        {
                            if (!leftRayFinal)
                            {
                                velX += velAcc;

                            }

                            else
                            {
                                velX = 0;
                            }

                        }
                        //If already moving right
                        else if (velX > 0)
                        {
                            if (!rightRayFinal)
                            {
                                if (velX < maxVel)
                                {
                                    velX += velAcc;
                                }
                                else
                                {
                                    velX = maxVel;
                                }
                            }
                            else
                            {
                                velX = 0;
                            }

                        }
                        //If not moving at all
                        else
                        {
                            if (!rightRayFinal)
                            {
                                if (velX < maxVel)
                                {
                                    velX += velAcc;
                                }
                                else
                                {
                                    velX = maxVel;
                                }
                            }
                            else
                            {
                                velX = 0;
                            }
                        }





                    }
                    //Both Left and Right btns are down
                    else if (left && right)
                    {

                        //if already moving left

                        if (velX < 0)
                        {

                            //if (lastDir == -Vector2.right)
                            //{

                            if (!leftRayFinal)
                            {

                                velX += velAcc;

                            }
                            else
                            {
                                velX = 0;
                            }


                        }

                        //if already moving right

                        else if (velX > 0)
                        {
                            if (!rightRayFinal)
                            {

                                velX -= velAcc;

                            }
                            else
                            {
                                velX = 0;
                            }


                        }

                        //if not moving at all

                        else
                        {

                        }

                    }

                        //Neither Left or Right are down
                    else
                    {


                        if (lastDir == Vector2.right)
                        {

                            if (!rightRayFinal)
                            {
                                if (velX > 0)
                                {
                                    velX -= velAcc;
                                }
                                else
                                {
                                    velX = 0;
                                }
                            }
                            else
                            {
                                velX = 0;
                            }

                        }
                        else if (lastDir == -Vector2.right)
                        {
                            if (!leftRayFinal)
                            {

                                if (velX < 0)
                                {
                                    velX += velAcc;
                                }
                                else
                                {
                                    velX = 0;
                                }
                            }
                            else
                            {
                                velX = 0;
                            }

                        }

                    }

                    //Manage Jump Input

                    if (launchJump)
                    {


                        if (!upRayFinal)
                        {

                            if (!jumpClimax)
                            {
                                //Debug.Log("Y=" + velY);

                                if (!addJumpForce)
                                {
                                    
                                    jumpTimer += Time.deltaTime;
                                   // Debug.Log("JT=" + jumpTimer);
                                    
                                    if (jumpTimer >= jumpDelay)
                                    {
                                        //velY = jumpHeight;
                                        
                                        addJumpForce = true;
                                        
                                                                          
                                    }




                                }
                                else
                                {

                                    if (!reachHeight)
                                    {
                                        velY += jumpInclineVel;

                                        if (velY >= jumpHeight)
                                        {
                                            reachHeight = true;
                                        }

                                        if (upRayFinal)
                                        {
                                            velY = 0;
                                            jumpTimer = 0;
                                            jumpClimax = true;
                                            reachHeight = false;
                                        }

                                    }
                                    else
                                    {
                                        velY -= jumpDeclineVel;

                                        if (velY <= 0)
                                        {

                                            jumpClimax = true;
                                            
                                        }

                                        if (upRayFinal)
                                        {
                                            velY = 0;
                                            jumpTimer = 0;
                                            jumpClimax = true;
                                            reachHeight = false;
                                        }


                                    }





                                    //if (upRayFinal)
                                    //{
                                    //    velY = 0;
                                    //    jumpClimax = true;

                                    //    Debug.Log("CEILING");
                                        
                                    //}
                                    
                                    //velY -= jumpVel;
                                    
                                    //if (velY <= 0)
                                    //{

                                    //    jumpClimax = true;
                                    //}
                                }

                               


                            }
                            else
                            {
                                addJumpForce = false;
                                launchJump = false;
                                jumpClimax = false;
                                jumpTimer = 0;
                                reachHeight = false;
                            }
                        }
                        else
                        {
                            addJumpForce = false;
                            launchJump = false;
                            jumpClimax = false;
                            jumpTimer = 0;
                            reachHeight = false;
                        }


                    }


                                       //Cache rigidbody2D velocity

                    Vector2 velCache = GetComponent<Rigidbody2D>().velocity;

                    //Apply X velocity


                    velCache.x = velX;
                    velCache.y = velY;

                    //Limit Y velocity


                    if (velCache.y < -maxFallSpeed)
                    {
                        velCache.y = -maxFallSpeed;

                    }




                    //if (velCache.y > 0)
                    //{

                    //    if (!upRayFinal)
                    //    {

                    //        if (velCache.y > maxJumpHeight)
                    //        {
                    //            velCache.y = maxJumpHeight;
                    //        }
                    //    }
                    //    else
                    //    {
                    //        velCache.y = -0.5f;
                    //    }
                    //}
                    //else if (velCache.y < 0)
                    //{

                    //    if (velCache.y < -maxFallSpeed)
                    //    {
                    //        velCache.y = -maxFallSpeed;
                    //    }

                    //}
                    //else
                    //{
                    //    //Do nothing
                    //}



                    //Apply Velocity Changes

                    GetComponent<Rigidbody2D>().velocity = velCache;


                    break;


                case States.Status:
                    break;


            }
        }







    }




    public void InitializeMoveColliders()
    { 
        foreach (Transform child in transform)
        {
            if (child.gameObject.CompareTag("MoveCollider"))
            {

                MoveColliderCheck script = child.GetComponent<MoveColliderCheck>();


                switch (script.type)
                {

                    case MoveColliderType.CEILING:

                        ceilingColliders.Add(script);
                        break;

                    case MoveColliderType.GROUND:


                        groundColliders.Add(script);

                        break;

                    case MoveColliderType.LEFT:

                        leftColliders.Add(script);

                        break;

                    case MoveColliderType.RIGHT:
                        rightColliders.Add(script);
                        break;

                }

            }
        }




        groundColResults = new bool[groundColliders.ToArray().Length];
        ceilingColResults = new bool[ceilingColliders.ToArray().Length];
        rightColResults = new bool[rightColliders.ToArray().Length];
        leftColResults = new bool[leftColliders.ToArray().Length];
        Debug.Log("COLLECTED=" + ceilingColliders.ToArray().Length);
    }


    public void CheckMoveColliders()
    {
        int i = 0;
        //Check Ground
        foreach (MoveColliderCheck check in groundColliders)
        {

            groundColResults[i] = check.hasCollision;
            i++;

        }
        downRayFinal = false;


        for (int a = 0; a < groundColResults.Length; a++)
        {

            if (groundColResults[a])
            {


                downRayFinal = groundColResults[a];
            }
        }

        //Check Ceiling


        i = 0;

        foreach (MoveColliderCheck check in ceilingColliders)
        {

            ceilingColResults[i] = check.hasCollision;
            i++;

        }
        upRayFinal = false;


        for (int a = 0; a < ceilingColResults.Length; a++)
        {

            if (ceilingColResults[a])
            {


                upRayFinal = ceilingColResults[a];
            }
        }


        //Check Right

        i = 0;
        foreach (MoveColliderCheck check in rightColliders)
        {

            rightColResults[i] = check.hasCollision;
            i++;

        }
        rightRayFinal = false;


        for (int a = 0; a < rightColResults.Length; a++)
        {

            if (rightColResults[a])
            {


                rightRayFinal = rightColResults[a];
            }
        }

        //Check Left

        i = 0;
        foreach (MoveColliderCheck check in leftColliders)
        {

            leftColResults[i] = check.hasCollision;
            i++;

        }
        leftRayFinal = false;


        for (int a = 0; a < leftColResults.Length; a++)
        {

            if (leftColResults[a])
            {


                leftRayFinal = leftColResults[a];
            }
        }

    }


    public void ResetColliders()
    {
        Debug.Log("RESET PLAYER");

            
        foreach(MoveColliderCheck check in leftColliders)
        {
            check.Reset();

        }

        foreach (MoveColliderCheck check in rightColliders)
        {
            check.Reset();
        }
    


    //public List<MoveColliderCheck> rightColliders = new List<MoveColliderCheck>();




    }

    public void Pause()
    {
        
        if (!isPaused)
        {

            cacheY = GetComponent<Rigidbody2D>().velocity.y;

            playerAnim.speed = 0;
            
            GetComponent<Rigidbody2D>().isKinematic = true;
            isPaused = true;
            
        }
        else
        {
            playerAnim.speed = 1;
            GetComponent<Rigidbody2D>().isKinematic = false;
            isPaused = false;

            Vector2 tmp = GetComponent<Rigidbody2D>().velocity;
            tmp.y = cacheY;
            GetComponent<Rigidbody2D>().velocity = tmp;
            
        }
        
    }


}
