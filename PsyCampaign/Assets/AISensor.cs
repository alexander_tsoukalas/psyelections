﻿using UnityEngine;
using System.Collections;

public class AISensor : MonoBehaviour {

	// Use this for initialization


    public bool detect=false;

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    void OnTriggerEnter2D(Collider2D d)
    {
        if (d.gameObject.CompareTag("Player"))
        {

            detect = true;
        }

    }

    void OnTriggerStay2D(Collider2D d)
    {
        if (d.gameObject.CompareTag("Player"))
        {

            detect = true;
        }
    }

    void OnTriggerExit2D(Collider2D d)
    {

        if (d.gameObject.CompareTag("Player"))
        {

            detect = false;
        }

    }

}
