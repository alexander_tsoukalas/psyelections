﻿using UnityEngine;
using System.Collections;

public class SpecialSensor : MonoBehaviour {

	// Use this for initialization


    public int target = 0;
    public bool triggered=false;



	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    void OnTriggerEnter2D(Collider2D d)
    {


         switch (target)
        {

            case 0:

                if (d.gameObject.CompareTag("Player"))
                {
                    triggered = true;

                }

                break;


            case 1:

                if (d.gameObject.name == "Bolek")
                {
                    triggered = true;

                }

                break;


            case 2:

                if (d.gameObject.name == "Lolek")
                {

                    triggered = true;
                }

                break;



        }

    }
       
        


    void OnTriggerExit2D(Collider2D d)
    {
        switch (target)
        {

            case 0:

                if (d.gameObject.CompareTag("Player"))
                {
                    triggered = false;

                }

                break;


            case 1:

                if (d.gameObject.name == "Bolek")
                {
                    triggered = false;

                }

                break;


            case 2:

                if (d.gameObject.name == "Lolek")
                {

                    triggered = false;
                }

                break;



        }

    }

}
