﻿using UnityEngine;
using System.Collections;

public class MovingPlatform : MonoBehaviour {

    public enum Modes { UP_DOWN, LEFT_RIGHT, PLATTRAP1, PLATTRAP2, PLATTRAP3,B_LIFT };

    public Modes mode;

    public bool playerOn = false;
    private GameObject player;
    Transform pos;
    Animator anim;
    public bool isPaused;
    public Transform trigger;
    private Switch triggerSW;
    private bool hasTrigger;

	// Use this for initialization
	void Awake () {
        player = GameObject.FindGameObjectWithTag("Player");
        pos = transform.parent.FindChild("pos");
        anim = GetComponentInParent<Animator>();

        if (trigger != null)
        {
            triggerSW = trigger.GetComponent<Switch>();
            hasTrigger = true;
        }

        switch (mode)
        {
            case Modes.UP_DOWN:

                anim.SetInteger("Index", 0);

                break;


            case Modes.LEFT_RIGHT:
                anim.SetInteger("Index", 1);
                break;

            case Modes.PLATTRAP1:
                anim.SetInteger("Index", 2);
                break;

            case Modes.PLATTRAP2:
                anim.SetInteger("Index", 3);
                break;

            case Modes.PLATTRAP3:
                anim.SetInteger("Index", 4);
                break;

            case Modes.B_LIFT:
                anim.SetInteger("Index", 5);
                break;

        }



    }

    void Update()
    {
        if (hasTrigger)
        {
            anim.SetBool("Activate", triggerSW.isTriggered);


        }

    }




	// Update is called once per frame
	void FixedUpdate () {



        if (playerOn)
        {
            //player.transform.position = transform.position;

        }


	}



    void OnTriggerEnter2D(Collider2D d)
    {

        if(d.gameObject.CompareTag("Player")||d.gameObject.CompareTag("NPC"))
        {
            playerOn = true;

            d.transform.SetParent(pos);
        }


    }

    void OnTriggerExit2D(Collider2D d)
    {


        if (d.gameObject.CompareTag("Player")||d.gameObject.CompareTag("NPC"))
        {
            playerOn = false;
            d.transform.parent = null;
        }


    }



    public void Pause()
    {
        if (isPaused)
        {

            anim.speed = 1;
            isPaused = false;
        }
        else
        {
            anim.speed = 0;
            isPaused = true;
        }
    }

}
